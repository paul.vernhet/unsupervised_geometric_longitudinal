

cpu_no = 2

def print_line_cross(noise_level):
    print('')
    global cpu_no
    cpu_per_task = 2
    kill_sessions = []
    for fold in range(10):
        session_name = 'cross_noise_{}_fold_{}'.format(noise_level, fold)
        scripts_args = '-fold {} -noise {}'.format(fold, noise_level)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print('tmux new -d -s "{}" \'taskset -c {} python run_synthetic_cross.py {}  > ../logs/log_{}.txt \' '.format(session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)

def print_line_cross_variational(noise_level):
    print('')
    global cpu_no
    cpu_per_task = 1
    kill_sessions = []
    for fold in range(10):
        session_name = 'cross_variational_noise_{}_fold_{}'.format(noise_level, fold)
        scripts_args = '-fold {} -noise {}'.format(fold, noise_level)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print('tmux new -d -s "{}" \'taskset -c {} python run_synthetic_cross_variational.py {}  > ../logs/log_{}.txt \' '.format(session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    #for elt in kill_sessions:
    #    print(elt)



def print_lines_adas_folds():
    print('')
    global cpu_no
    cpu_per_task = 2
    kill_sessions = []
    for fold in range(10):
        session_name = 'adas_fold_{}'.format(fold)
        scripts_args = '-fold {}'.format(fold)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print('tmux new -d -s "{}" \'taskset -c {} python run_adas.py {}  > ../logs/log_{}.txt \' '.format(session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)

def print_lines_adas_folds_l(l):
    print('')
    global cpu_no
    cpu_per_task = 2
    kill_sessions = []
    for fold in range(10):
        session_name = 'adas_l_{}_fold_{}'.format(l, fold)
        scripts_args = '-fold {} -l {}'.format(fold, l)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print('tmux new -d -s "{}" \'taskset -c {} python run_adas.py {}  > ../logs/log_{}.txt \' '.format(
            session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)


def print_lines_adas_test_size():
    print('')
    global cpu_no
    cpu_per_task = 2
    kill_sessions = []
    for fold in range(10):
        session_name = 'test_size_adas_fold_{}'.format(fold)
        scripts_args = '-fold {}'.format(fold)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print('tmux new -d -s "{}" \'taskset -c {} python run_adas_variational.py {}  > ../logs/log_{}.txt \' '.format(session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)


def print_lines_adas_nb_obs():
    print('')
    global cpu_no
    cpu_per_task = 1
    kill_sessions = []
    for fold in range(10):
        session_name = 'nb_obs_adas_fold_{}'.format(fold)
        scripts_args = '-fold {}'.format(fold)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print('tmux new -d -s "{}" \'taskset -c {} python run_adas_variational.py {}  > ../logs/log_{}.txt \' '.format(session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)


def print_lines_adas_randomized_nb_obs():
    print('')
    global cpu_no
    cpu_per_task = 2
    kill_sessions = []
    for fold in range(10):
        session_name = 'randomized_nb_obs_adas_fold_{}'.format(fold)
        scripts_args = '-fold {}'.format(fold)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print('tmux new -d -s "{}" \'taskset -c {} python run_adas.py {}  > ../logs/log_{}.txt \' '.format(session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)


def print_lines_adas_folds_variational():
    print('')
    global cpu_no
    cpu_per_task = 1
    kill_sessions = []
    for fold in range(10):
        session_name = 'adas_variational_fold_{}'.format(fold)
        scripts_args = '-fold {}'.format(fold)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print('tmux new -d -s "{}" \'taskset -c {} python run_adas_variational.py {}  > ../logs/log_{}.txt \' '.format(session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)

def print_lines_synthetic_dataset_multimodal():
    print('')
    global cpu_no
    cpu_per_task = 2
    kill_sessions = []
    for fold in range(10):
        session_name = 'synthetic_multimodal_fold_{}'.format(fold)
        scripts_args = '-fold {}'.format(fold)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print(
            'tmux new -d -s "{}" \'taskset -c {} python run_multimodal_cross.py {}  > ../logs/log_{}.txt \' '.format(
                session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)

def print_lines_mri_scores_multimodal():
    print('')
    global cpu_no
    cpu_per_task = 2
    kill_sessions = []
    for fold in range(10):
        session_name = 'multimodal_det_scores_fold_{}'.format(fold)
        scripts_args = '-fold {}'.format(fold)
        cpu = '{}-{}'.format(cpu_no, cpu_no + cpu_per_task)
        print(
            'tmux new -d -s "{}" \'taskset -c {} python run_multimodal_det_scores_ppmi.py {}  > ../logs/log_{}.txt \' '.format(
                session_name, cpu, scripts_args, session_name))
        kill_sessions.append('killSession {}'.format(session_name))
        cpu_no += cpu_per_task
    for elt in kill_sessions:
        print(elt)

#print_lines_adas_folds()
# print_lines_adas_folds_l(0.1)
# print_lines_adas_test_size()
#print_lines_adas_nb_obs()`
# print_lines_adas_randomized_nb_obs()


# print_line_cross(0.)
# print_line_cross(0.02)
# print_line_cross(0.05)
# print_line_cross(0.1)

#print_lines_adas_folds_variational()

# print_line_cross_variational(0.0)
# print_line_cross_variational(0.02)
# print_line_cross_variational(0.05)
# print_line_cross_variational(0.1)


print_lines_synthetic_dataset_multimodal()
#print_lines_mri_scores_multimodal()