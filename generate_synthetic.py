import os
import numpy as np


nb_individuals = 200
nb_visits = 5
data_path = 'data'

ids = []
values = []
hidden_values = []
times = []

for i in range(nb_individuals):
    a = np.exp(np.random.normal(0., 0.1, size=2))
    b = np.random.normal(size=2)
    min_time = np.random.uniform(-1.5, -0.5)
    max_time = np.random.uniform(0.5, 1.5)
    times_indiv = np.linspace(min_time, max_time, nb_visits)
    hidden_values.append([a, b])
    for t in times_indiv:
        ids.append(i)
        value = np.exp(t * a) + b
        values.append(value)
        times.append(t)

assert len(times) == len(values)
assert len(values) == len(ids)
assert nb_individuals == len(hidden_values)

# Now saving the result into npy arrays.
ids = np.array(ids)
values = np.array(values)
hidden_values = np.array(hidden_values)
times = np.array(times)

np.save(os.path.join(data_path, 'ids.npy'), ids)
np.save(os.path.join(data_path, 'values.npy'), values)
np.save(os.path.join(data_path, 'hidden_values.npy'), hidden_values)
np.save(os.path.join(data_path, 'times.npy'), times)
