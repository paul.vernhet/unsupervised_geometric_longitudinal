from torch.optim import Adam
from torch.utils.data import DataLoader
import torch
import numpy as np
from utils import reparametrize, plot_progression, initialize_autoencoder, initialize_variational
from torch.optim.lr_scheduler import StepLR
import os


def get_test_statistics(model, test_dataset, train_noise_variance, randomize_nb_obs=True):
    noise_variance = {}
    values_batch = {}
    latent_traj_batch = {}
    for key in model.decoders.keys():
        values_batch[key] = torch.zeros(0).type(test_dataset.type)
        latent_traj_batch[key] = torch.zeros(0).type(test_dataset.type)
    means_batch = torch.zeros((len(test_dataset.rids), model.encoder_dim)).type(test_dataset.type)
    log_variances_batch = None
    if model.variational:
        log_variances_batch = torch.zeros((len(test_dataset.rids), model.encoder_dim)).type(test_dataset.type)
    test_dataloader = DataLoader(test_dataset)
    no_in_batch = 0
    test_loss = 0
    regularity_loss = 0
    for i, data in enumerate(test_dataloader):

        # Get the latent trajectories (here batch is all test subjects)
        if model.variational:
            mean, log_variances = model.encode(data, randomize_nb_obs=randomize_nb_obs)  # mean is the latent variable
            latent_trajectories = model.get_latent_trajectories(data, mean, log_variances=log_variances, sample=True)
        else:
            mean = model.encode(data, randomize_nb_obs=randomize_nb_obs)
            latent_trajectories = model.get_latent_trajectories(data, mean)

        # Accumulating the observations for the batch
        for key, val in data.items():
            if key != 'idx':
                values_batch[key] = torch.cat([values_batch[key], val['values'].squeeze(0)], 0)

        # Accumulating the latent trajectories for the batch:
        for key, val in latent_trajectories.items():
            latent_traj_batch[key] = torch.cat([latent_traj_batch[key], val], 0)

        means_batch[no_in_batch] = mean
        if model.variational:
            log_variances_batch[no_in_batch] = log_variances
        no_in_batch += 1

    # Decode the trajectories

    reconstructed = model.decode(latent_traj_batch)

    # Computing the loss
    reconstruction = torch.zeros(1)
    if test_dataset.use_cuda:
        reconstruction = reconstruction.cuda()

    for key, val in reconstructed.items():
        assert val.size() == values_batch[key].size()
        # Mse error for the given modality
        mse_error = torch.sum((val - values_batch[key]) ** 2)

        # Weighted mse error for the modality
        reconstruction += mse_error / train_noise_variance[key]

        noise_variance[key] = mse_error.cpu().detach().numpy() / val.numel()

    regularity = model.compute_regularity(means_batch, log_variances_batch)

    # Total loss
    loss = regularity + reconstruction
    test_loss += loss.cpu().detach().numpy()
    regularity_loss += regularity.cpu().detach().numpy()


    return noise_variance, regularity_loss, test_loss



def estimate_random_slope_model_variational(model, dataset, n_epochs=10000,
                                learning_rate=5e-4, output_dir='output',
                                batch_size=32, save_every_n_iters=50,
                                print_every_n_iters=1, dico={},
                                initialize_iterations=50,
                                call_back=None, lr_decay=0.95, l=0.1,
                                estimate_noise=True, randomize_nb_obs=False,
                                            test_dataset=None, keys_to_initialize=[]):

    dataloader = DataLoader(dataset, batch_size=1, shuffle=True)

    if test_dataset is not None:
        test_dataloader = DataLoader(test_dataset, batch_size=1, shuffle=True)

    #if keys_to_initialize is not None:
    for key in keys_to_initialize:
        initialize_autoencoder(model.encoders[key], model.decoders[key],
                               dataloader, model.type, key, n_epochs=20)


    # For now, we initialize the encoder to give the mean of the prior parameters and its variance !
    if model.variational:
        initialize_variational(dataloader, model)

    # if initialize_iterations > 0 and model.is_image:
    #     initialize_autoencoder(model.encoder, model.decoder, dataloader, dataset.type, iterations=initialize_iterations)

    print('l {}, lr {}, decay {}, batch_size {}'.format(l, learning_rate, lr_decay, batch_size))

    optimizer = Adam(model.parameters(), lr=learning_rate, weight_decay=1e-3)

    scheduler = StepLR(optimizer, step_size=max(10, int(n_epochs/50)), gamma=lr_decay)

    # Train
    train_losses = []
    reconstruction_losses = {}
    for key in model.data_info.keys():
        reconstruction_losses[key] = []
    regularity_losses = []

    # test
    test_losses = []
    test_reconstruction_losses = {}
    for key in model.data_info.keys():
        test_reconstruction_losses[key] = []
    test_regularity_losses = []

    time_shift_stds = []
    eta_stds = []
    time_shift_means = []
    eta_means = []

    noise_variance = {}
    for key in model.data_info.keys():
        noise_variance[key] = l

    for epoch in range(n_epochs):
        scheduler.step()

        train_loss = 0.
        regularity_loss = 0.

        no_in_batch = 0
        n_batch = 0

        values_batch = {}
        latent_traj_batch = {}
        for key in model.decoders.keys():
            values_batch[key] = torch.zeros(0).type(dataset.type)
            latent_traj_batch[key] = torch.zeros(0).type(dataset.type)

        means_batch = torch.zeros((batch_size, model.encoder_dim)).type(dataset.type)
        log_variances_batch = None
        if model.variational:
            log_variances_batch = torch.zeros((batch_size, model.encoder_dim)).type(dataset.type)

        min_lp_x, max_lp_x = float('inf'), -float('inf')

        for i, data in enumerate(dataloader):

            if model.variational:
                mean, log_variances = model.encode(data, randomize_nb_obs=randomize_nb_obs) # mean is the latent variable
                latent_trajectories = model.get_latent_trajectories(data, mean, log_variances=log_variances, sample=True)
            else:
                mean = model.encode(data, randomize_nb_obs=randomize_nb_obs)
                latent_trajectories = model.get_latent_trajectories(data, mean)

            # Accumulating the observations for the batch
            for key, val in data.items():
                if key != 'idx':
                    values_batch[key] = torch.cat([values_batch[key], val['values'].squeeze(0)], 0)

            # Accumulating the latent trajectories for the batch:
            for key, val in latent_trajectories.items():
                latent_traj_batch[key] = torch.cat([latent_traj_batch[key], val], 0)

            means_batch[no_in_batch] = mean
            if model.variational:
                log_variances_batch[no_in_batch] = log_variances

            no_in_batch += 1

            # We have gathered batch_size different subjects OR we are at the end of the dataset, we optimize:
            if no_in_batch % batch_size == 0 or i == len(dataloader)-1:

                # Zero-ing the gradients
                optimizer.zero_grad()

                # Decoding
                reconstructed = model.decode(latent_traj_batch)

                # Computing the loss
                reconstruction = torch.zeros(1)
                if dataset.use_cuda:
                    reconstruction = reconstruction.cuda()

                for key, val in reconstructed.items():
                    assert val.size() == values_batch[key].size()
                    # Mse error for the given modality
                    mse_error = torch.sum((val - values_batch[key]) ** 2)

                    # Weighted mse error for the modality
                    if estimate_noise:
                        reconstruction += mse_error / noise_variance[key]
                    else:
                        reconstruction += mse_error / l

                    # update of the noise variance:
                    noise_variance[key] = mse_error.cpu().detach().numpy() / val.numel()


                # Truncating in case batch was not full size:
                means_batch = means_batch[:no_in_batch]
                if model.variational:
                    log_variances_batch = log_variances_batch[:no_in_batch]

                #if epoch > 50:
                # Computing regularity (simple or variational, the model knows)
                regularity = model.compute_regularity(means_batch, log_variances_batch)
                #else:
                #   regularity = torch.zeros(1)

                # Total loss
                loss = regularity + reconstruction

                # Step of gradient descent
                loss.backward()
                optimizer.step()

                # Storing for print and plot
                train_loss += loss.cpu().detach().numpy()
                regularity_loss += regularity.cpu().detach().numpy()

                # Updating the bounds in x positions in the latent space, to know from where and until where to plot.
                for val in latent_trajectories.values():
                    min_lp_x = min(np.min(val[:, 0].cpu().detach().numpy()), min_lp_x)
                    max_lp_x = max(np.max(val[:, 0].cpu().detach().numpy()), max_lp_x)

                # Zero-ing all storage variables for the batch
                values_batch = {}
                latent_traj_batch = {}
                for key in model.decoders.keys():
                    values_batch[key] = torch.zeros(0).type(dataset.type)
                    latent_traj_batch[key] = torch.zeros(0).type(dataset.type)

                means_batch = torch.zeros((batch_size, model.encoder_dim)).type(dataset.type)
                if model.variational:
                    log_variances_batch = torch.zeros((batch_size, model.encoder_dim)).type(dataset.type)

                no_in_batch = 0
                n_batch += 1

        train_losses.append(train_loss/(i))
        for key in reconstruction_losses.keys():
            if estimate_noise:
                reconstruction_losses[key].append(np.sqrt(noise_variance[key])) # this is the mse on the latest batch, it's not over all the dataset
        regularity_losses.append(regularity_loss/(i))

        time_shift_stds.append(model.get_time_shift_std())
        time_shift_means.append(model.get_time_shift_mean())
        eta_stds.append(model.get_eta_std())
        eta_means.append(model.get_eta_mean())

        # Compute the test regularity/reconstructions/loss
        if test_dataset is not None:
            test_noise_variance, test_regularity, test_loss = get_test_statistics(model, test_dataset, noise_variance, randomize_nb_obs=True)

            test_losses.append(test_loss/len(test_dataset.rids))
            for key in reconstruction_losses.keys():
                test_reconstruction_losses[key].append(np.sqrt(test_noise_variance[key]))
            test_regularity_losses.append(test_regularity/len(test_dataset.rids))


        if epoch % print_every_n_iters == 0:

            # Learning rate decay
            lr = 0.
            for param_group in optimizer.param_groups:
                lr = [param_group['lr']][0]
                break

            print('{}/{}, train loss {}, regularity loss {}, lr {}, empirical mse {}'.format(epoch, n_epochs, train_loss, regularity_loss, lr, noise_variance))

        # We save a plot of 4 individuals at the end of each epoch
        if epoch % save_every_n_iters == 0 or epoch == n_epochs-1:


            # Plot of the training loss
            plot_progression([train_losses, regularity_losses] + [reconstruction_losses[key] for key in reconstruction_losses.keys()],
                             ['train', 'regularity'] + ['reconstruction_' + key for key in reconstruction_losses.keys()], output_dir, 'train_loss.pdf')
            if test_dataset is not None:
                plot_progression([train_losses, regularity_losses] + [reconstruction_losses[key] for key in
                                                                      reconstruction_losses.keys()],
                                 ['loss', 'regularity'] + ['reconstruction_' + key for key in
                                                            reconstruction_losses.keys()], output_dir, 'test_loss.pdf',
                                 test_l=[test_losses, test_regularity_losses] + [test_reconstruction_losses[key] for key in test_reconstruction_losses.keys()])
                # Plot of the prior parameters on eta and tau
            plot_progression([eta_means, eta_stds, time_shift_means, time_shift_stds], ['eta mean', 'eta std', 'time shift mean', 'time shift std'],
                             output_dir, 'prior_parameters.pdf')

            # Getting and plotting some reconstructions for training set
            times, real, reconstructed = model.get_reconstructions(dataloader)
            model.plot_reconstructions(times, real, reconstructed, output_dir, split="train")

            # Getting and plotting some reconstructions for testing set
            if test_dataset is not None:
                times, real, reconstructed = model.get_reconstructions(test_dataloader)
                model.plot_reconstructions(times, real, reconstructed, output_dir, split="test")

            del times
            del real
            del reconstructed

            # Plotting the average trajectory
            model.plot_average_trajectory(output_dir, min_x=min_lp_x, max_x=max_lp_x)

            # Saving the model
            model.save(output_dir)

            # Do extra user-required stuff
            if call_back is not None:
                 call_back(model)

    # Final save of the losses
    np.save(os.path.join(output_dir, 'train_losses.npy'), train_losses)
    np.save(os.path.join(output_dir, 'reconstruction_losses.npy'), reconstruction_losses)
    np.save(os.path.join(output_dir, 'regularity_losses.npy'), regularity_losses)



