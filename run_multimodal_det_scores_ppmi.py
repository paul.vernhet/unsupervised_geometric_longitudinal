import os
import sys

from longitudinal_scalar_dataset import LongitudinalScalarDataset
from longitudinal_image_dataset import LongitudinalImageDataset
from random_slope_model import RandomSlopeModel
from estimate_variational import estimate_random_slope_model_variational
import numpy as np
from multimodal_dataset import MultimodalDataset, DatasetTypes
from torch.utils.data import DataLoader
import argparse
import pandas as pd
from sklearn.model_selection import KFold

parser = argparse.ArgumentParser()


parser.add_argument("-cuda", "-cuda", help='(on/off): attempt to compute on gpu.', default='off')
parser.add_argument("-fold", '-fold', default='0')
args = parser.parse_args()

use_cuda = True if args.cuda == 'on' else False
fold = int(args.fold)

output_dir = '../output/output_multimodal_det_scores_ppmi/'

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

################## Getting the images ###################################@

info_images = pd.read_csv('../data/data_ppmi/df_images.csv')
info_images = info_images.sort_values(['Subject', 'Age'])

data_dir = '../data/data_ppmi/'

image_width = 32
image_dimension = 3
image_shape = (image_width, image_width, image_width)

images_path = info_images['path'].values
images_path = [os.path.join(data_dir, elt) for elt in images_path]
times_det = info_images['Age at Visit'].values
ids_det = info_images['Subject'].values

labels_det = {}
for rid in ids_det:
    labels_det[rid] = 0

###################### Getting the scores ##############################

df = pd.read_csv('../data/data_ppmi/df_scores.csv')
df = df.sort_values(['Subject Identifier', 'Age at Visit'])

values_scores = df.values[:, 3] / 108.
values_scores = values_scores.reshape(-1, 1)
ids_scores = df['Subject Identifier'].values
times_scores = df['Age at Visit'].values


################## Keeping only the ids with observations in the two modalities ##############
distinct_rids = np.array(list(set(np.intersect1d(ids_det, ids_scores))))



########################## Constituting the folds #####################################
kf = KFold(n_splits=10, random_state=0, shuffle=True)

for k, (train_patients_index, test_patients_index) in enumerate(kf.split(distinct_rids)):
    if k == fold:
        train_patients, test_patients = distinct_rids[train_patients_index], distinct_rids[test_patients_index]
        folds = {'train': train_patients, 'test': test_patients}
        break

# Scores

scores_train_dataset_ = LongitudinalScalarDataset(list(ids_scores[np.isin(ids_scores, folds['train'])]),
                                          list(values_scores[np.isin(ids_scores, folds['train'])]),
                                          list(times_scores[np.isin(ids_scores, folds['train'])]),
                                            use_cuda=use_cuda)

scores_test_dataset_ = LongitudinalScalarDataset(list(ids_scores[np.isin(ids_scores, folds['test'])]),
                                          list(values_scores[np.isin(ids_scores, folds['test'])]),
                                          list(times_scores[np.isin(ids_scores, folds['test'])]),
                                         ages_std=scores_train_dataset_.ages_std,
                                         ages_mean=scores_train_dataset_.ages_mean,
                                         use_cuda=use_cuda)

images_train_dataset_ = LongitudinalImageDataset(
    ids=list(ids_det[np.isin(ids_det, folds['train'])]),
    images_path=[images_path[i] for i in np.array(list(range(len(times_det))))[np.isin(ids_det, folds['train'])]],
    times=list(times_det[np.isin(ids_det, folds['train'])]),
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    ages_std=scores_train_dataset_.ages_std,
    ages_mean=scores_train_dataset_.ages_mean,
    is_det_image=True,
    image_255=False
)

images_test_dataset_ = LongitudinalImageDataset(
    ids=list(ids_det[np.isin(ids_det, folds['test'])]),
    images_path=[images_path[i] for i in np.array(list(range(len(times_det))))[np.isin(ids_det, folds['test'])]],
    times=list(times_det[np.isin(ids_det, folds['test'])]),
    image_shape=image_shape,
    image_dimension=image_dimension,
    use_cuda=use_cuda,
    ages_std=scores_train_dataset_.ages_std,
    ages_mean=scores_train_dataset_.ages_mean,
    is_det_image=True,
    image_255=False
)


# Multimodal dataset
train_dataset = MultimodalDataset([scores_train_dataset_, images_train_dataset_], ['scores', 'det'],
                                  [DatasetTypes.SCALAR, DatasetTypes.IMAGE])

train_dataset.print_dataset_statistics()

test_dataset = MultimodalDataset([scores_test_dataset_, images_test_dataset_], ['scores', 'det'],
                                  [DatasetTypes.SCALAR, DatasetTypes.IMAGE])


#%%


# Dataset Type / encoder hidden dim / decoder hidden dim / data_dim / labels / colors


score_dim = 1

c1 =  (17/255.,72/255.,107./255.)
c2 = (71/255.,131/255.,86/255.)
c3 = (172/255.,43/255.,73/255.)
c4 = (218/255., 99/255., 40/255.)
c5 = (255/255., 164/255., 37/255.)
colors = [c1]


data_info = {
    'scores': (DatasetTypes.SCALAR, 8, 8, score_dim,
                ['MDS_sum',
       'MDS_Rigidity', 'MDS_VoiceFace', 'MDS_Gait', 'MDS_Bradykinesia'], colors),

    'det': (DatasetTypes.IMAGE, 8, 8, image_shape,
                   None, None),
}


output_dir = os.path.join(output_dir, 'output_det_scores_{}'.format(fold))
print('Output directory', output_dir)
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))



#%% Launch

random_slope_model = RandomSlopeModel(
    data_info=data_info,
    latent_space_dim=5,
    pre_encoder_dim=8,
    random_slope=False,
    variational=False,
    use_cuda=use_cuda,
    atlas_path=None
)


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions, latent_trajectories = model.compute_residuals(dataloader)
    for key in ids.keys():
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_residuals.txt'), residuals[key])
        np.savetxt(os.path.join(output_dir, prefix + '_' + key + '_ids.txt'), ids[key])
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_trajectories.txt'), latent_trajectories)


def call_back(model):
    save_dataset_info(train_dataset, model, 'train', output_dir)
    save_dataset_info(test_dataset, model, 'test', output_dir)


estimate_random_slope_model_variational(random_slope_model, train_dataset, n_epochs=10000,
                                        learning_rate=1e-3, output_dir=output_dir,
                                        batch_size=32, save_every_n_iters=5,
                                        call_back=call_back, lr_decay=0.99,
                                        l=1e-3, estimate_noise=True,
                                        randomize_nb_obs=True,
                                        keys_to_initialize=['det'])


