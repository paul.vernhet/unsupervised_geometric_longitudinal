import torch
import torch.nn as nn


# abstract class for RNNs
class RNN(nn.Module):
    def __init(self):
        super(RNN, self).__init__()

    def _get_init_hidden(self, typ):
        return torch.zeros(self.hidden_dim).type(typ)

    def hidden_to_output(self, hidden):
        output_means = self.fc_means(torch.tanh(hidden))
        return hidden, output_means


class ScalarRNN(RNN):
    def __init__(self, in_dim, hidden_dim, out_dim):
        super(ScalarRNN, self).__init__()

        self.hidden_dim = hidden_dim
        self.fc_hidden = nn.Linear(hidden_dim, hidden_dim)
        self.fc_input = nn.Linear(in_dim, hidden_dim, bias=False)
        self.fc_means = nn.Linear(hidden_dim, out_dim)

        print('Scalar RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def _forward(self, input, hidden, last=True):
        hidden = torch.tanh(self.fc_hidden(hidden) + self.fc_input(input))

        if last:
            return self.hidden_to_output(hidden)

        else:
            return hidden, None

    def get_sequence_output(self, times, values):
        combined = torch.cat((times, values), 1)

        hidden = self._get_init_hidden(times.type())
        #assert len(combined) > 1, 'Sequence is of length one for the RNN, what is the behaviour ?'

        for elt in combined[:-1]:
            hidden, _ = self._forward(elt, hidden, last=False)

        _, means = self._forward(combined[-1], hidden)

        return means


class RCNN(RNN):
    def __init__(self, scalar_in_dim, hidden_dim, out_dim):
        super(RNN, self).__init__()
        self.scalar_rnn = ScalarRNN(scalar_in_dim, hidden_dim, out_dim)

    def get_sequence_output(self, times, values):
        assert len(times) == len(values)

        # First we perform the convolution on all the images:
        for layer in self.convolutions:
            values = layer(values)

        values = values.view(len(values), -1)

        # Then we feed the sequence to the scalar rnn
        means = self.scalar_rnn.get_sequence_output(times, values)

        return means

    def parameters(self):
        return list(self.scalar_rnn.parameters()) + list(self.convolutions.parameters())


class RCNN64(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN64, self).__init__(16*2*2 + 1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv2d(1, 8, 4, 4),  # 8 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv2d(16, 16, 2, 2),  # 16 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv2d(16, 16, 2, 2),  # 16 x 2 x 2
                                 nn.ReLU(),
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))


class RCNN3D64(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN3D64, self).__init__(16*2*2*2+1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv3d(1, 8, 4, 4),  # 8 x 16 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv3d(8, 16, 2, 2),  # 16 x 8 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv3d(16, 16, 2, 2),  # 16 x 4 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv3d(16, 16, 2, 2),  # 16 x 2 x 2 x 2
                                 nn.ReLU()
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))


class RCNN3D32(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN3D32, self).__init__(16*2*2*2+1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv3d(1, 8, 2, 2),  # 8 x 16 x 16 x 16
                                 nn.LeakyReLU(),
                                 nn.Conv3d(8, 16, 2, 2),  # 16 x 8 x 8 x 8
                                 nn.LeakyReLU(),
                                 nn.Conv3d(16, 16, 2, 2),  # 32 x 4 x 4 x 4
                                 nn.LeakyReLU(),
                                nn.Conv3d(16, 16, 2, 2),  # 16 x 2 x 2 x 2
                                nn.LeakyReLU(),
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))


class RCNN128(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN128, self).__init__(32*2*2+1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv2d(1, 8, 4, 4),  # 8 x 32 x 32
                                 nn.ReLU(),
                                 nn.Conv2d(8, 8, 2, 2),  # 8 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv2d(8, 16, 2, 2),  # 16 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv2d(16, 32, 2, 2),  # 32 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv2d(32, 32, 2, 2),  # 32 x 2 x 2
                                 nn.ReLU()
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))


class RCNN3D128(RCNN):
    def __init__(self, hidden_dim, out_dim, require_logvariances=False):
        super(RCNN3D128, self).__init__(32*2*2*2+1, hidden_dim, out_dim)

        self.convolutions = nn.ModuleList([nn.Conv3d(1, 8, 4, 4),  # 8 x 32 x 32 x 32
                                 nn.ReLU(),
                                 nn.Conv3d(8, 8, 2, 2),  # 8 x 16 x 16 x 16
                                 nn.ReLU(),
                                 nn.Conv3d(8, 16, 2, 2),  # 16 x 8 x 8 x 8
                                 nn.ReLU(),
                                 nn.Conv3d(16, 32, 2, 2),  # 32 x 4 x 4 x 4
                                 nn.ReLU(),
                                 nn.Conv3d(32, 32, 2, 2),  # 32 x 2 x 2 x 2
                                 nn.ReLU()
                                 ])

        print('Conv RNN has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))



class PreDecoder(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=32, depth="normal"):
        super(PreDecoder, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim),
                                     nn.Tanh()
                                     ])

        if depth == "shallow":
            self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                         nn.Tanh(),
                                         nn.Linear(hidden_dim, out_dim),
                                         nn.Tanh()
                                         ])


        print('Scalar pre decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


class MergeNetwork(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=32, depth="normal"):
        super(MergeNetwork, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim)
                                     ])

        if depth == "shallow":
            self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                         nn.Tanh(),
                                         nn.Linear(hidden_dim, hidden_dim),
                                         nn.Tanh(),
                                         nn.Linear(hidden_dim, out_dim)
                                         ])

        print('Merge network has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

class Decoder(nn.Module):

    def __init__(self, in_dim=2, out_dim=2, hidden_dim=32):
        super(Decoder, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     # nn.Tanh(),
                                     # nn.Linear(hidden_dim, hidden_dim), # one layer added for the folds experiments of adas
                                     nn.Tanh(),
                                     nn.Linear(hidden_dim, out_dim)
                                     ])

        print('Scalar decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


class Discriminator(nn.Module):

    def __init__(self, in_dim=2, hidden_dim=32):
        super(Discriminator, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, 1),
                                     nn.Sigmoid()
                                     ])

        print('Scalar discriminator has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))


    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


class Deconv64(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv64, self).__init__()
        self.fc = nn.Linear(in_dim, in_dim)
        self.ta = nn.Tanh()
        ngf = 2
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()
        self.layers = nn.ModuleList([
            nn.ConvTranspose2d(in_dim, 16 * ngf, 4, stride=4),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(4 * ngf, 2 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(2 * ngf, 1, 2, stride=2),
            last_function
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x = x.view(-1, self.in_dim, 1, 1)
        for layer in self.layers:
            x = layer(x)
        return x


class Deconv3D64(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv3D64, self).__init__()
        self.fc = nn.Linear(in_dim, 2*in_dim)
        self.ta = nn.Tanh()
        ngf = 2
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()

        self.layers = nn.ModuleList([
            nn.ConvTranspose3d(2*in_dim, 32 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(32*ngf, 16 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 1, 2, stride=2),
            last_function
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x = x.view(-1, 2*self.in_dim, 1, 1, 1)
        for layer in self.layers:
            x = layer(x)
        return x


class Deconv3D32(nn.Module):

    def __init__(self, in_dim=2, last_layer='relu'):
        self.in_dim = in_dim
        super(Deconv3D32, self).__init__()
        self.fc = nn.Linear(in_dim, 2*in_dim)
        self.ta = nn.Tanh()
        ngf = 3
        self.in_dim = in_dim
        last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()

        self.layers = nn.ModuleList([
            nn.ConvTranspose3d(2*in_dim, 32 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(32*ngf, 16 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(8 * ngf, 4 * ngf, 2, stride=2),
            nn.LeakyReLU(),
            nn.ConvTranspose3d(4 * ngf, 1, 2, stride=2),
            last_function
        ])
        print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))

    def forward(self, x):
        x = self.ta(self.fc(x))
        x = x.view(-1, 2*self.in_dim, 1, 1, 1)
        for layer in self.layers:
            x = layer(x)
        return x

# class Deconv128(nn.Module):
#
#     def __init__(self, in_dim=2, last_layer='relu'):
#         self.in_dim = in_dim
#         super(Deconv128, self).__init__()
#         self.fc = nn.Linear(in_dim, in_dim)
#         self.ta = nn.Tanh()
#         ngf = 2
#         self.in_dim = in_dim
#         last_function = nn.LeakyReLU() if last_layer == 'relu' else nn.Tanh()
#
#         self.layers = nn.ModuleList([
#             nn.ConvTranspose2d(in_dim, 32 * ngf, 4, stride=4),
#             nn.LeakyReLU(),
#             nn.ConvTranspose2d(32 * ngf, 16 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose2d(16 * ngf, 8 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose2d(8 * ngf, 4 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose2d(4 * ngf, 2 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose2d(2 * ngf, 1, 2, stride=2),
#             last_function
#         ])
#         print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))
#
#     def forward(self, x):
#         x = self.ta(self.fc(x))
#         x = x.view(-1, self.in_dim, 1, 1)
#         for layer in self.layers:
#             x = layer(x)
#         return x


# class Deconv3D128(nn.Module):
#
#     def __init__(self, in_dim=2):
#         self.in_dim = in_dim
#         super(Deconv3D128, self).__init__()
#         self.fc = nn.Linear(in_dim, in_dim)
#         self.ta = nn.Tanh()
#         ngf = 1
#         self.in_dim = in_dim
#         self.layers = nn.ModuleList([
#             nn.ConvTranspose3d(in_dim, 32 * ngf, 4, stride=4),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(32 * ngf, 16 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(16 * ngf, 8 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(8 * ngf, 4 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(4 * ngf, 2 * ngf, 2, stride=2),
#             nn.LeakyReLU(),
#             nn.ConvTranspose3d(2 * ngf, 1, 2, stride=2),
#             nn.LeakyReLU()
#         ])
#         print('Deconv decoder has {} parameters'.format(sum([len(elt.view(-1)) for elt in self.parameters()])))
#
#     def forward(self, x):
#         x = self.ta(self.fc(x))
#         x = x.view(-1, self.in_dim, 1, 1, 1)
#         for layer in self.layers:
#             x = layer(x)
#         return x