import os
import numpy as np
import torch.nn as nn
import torch
from networks import ScalarRNN, Decoder, RCNN64, RCNN3D64, Deconv64, Deconv3D64, RCNN128, RCNN3D128
import pickle
from torchvision.utils import save_image

import platform

if platform.system() == 'Linux':
    import matplotlib as matplotlib

    matplotlib.use('agg')

import matplotlib.pyplot as plt


class LongitudinalAutoencoder:

    def __init__(self, data_dim, latent_space_dim, is_image, encoder_hidden_dim=16, decoder_hidden_dim=32,
                 labels=None, use_cuda=False, image_width=64):
        self.data_dim = data_dim
        self.latent_space_dim = latent_space_dim
        self.is_image = is_image
        self.mse_loss = nn.MSELoss(size_average=False)
        self.labels = labels
        self.use_cuda = use_cuda
        self.encoder_dim = latent_space_dim
        self.colors = ['r', 'g', 'y', 'b', 'k']
        self.encoder_hidden_dim = encoder_hidden_dim
        self.decoder_hidden_dim = decoder_hidden_dim
        self.image_width = image_width

        if self.is_image:
            if self.data_dim == 2:
                self.value_shape = (1, self.image_width, self.image_width)
            else:
                self.value_shape = (1, self.image_width, self.image_width, self.image_width)

        else:
            self.value_shape = (self.data_dim,)

        last_layer = 'relu'

        # scalar case.
        if not is_image:
            self.encoder = ScalarRNN(in_dim=data_dim+1,
                               hidden_dim=encoder_hidden_dim,
                                     out_dim=self.encoder_dim)
            self.decoder = Decoder(in_dim=latent_space_dim + 1, out_dim=data_dim, hidden_dim=decoder_hidden_dim)

        # image case
        else:
            if self.data_dim == 2:
                if image_width == 64:
                    self.encoder = RCNN64(hidden_dim=encoder_hidden_dim, out_dim=self.encoder_dim )
                    self.decoder = Deconv64(in_dim=latent_space_dim+1, last_layer=last_layer)
                elif image_width == 128:
                    self.encoder = RCNN128(hidden_dim=encoder_hidden_dim, out_dim=self.encoder_dim)
                    self.decoder = Deconv128(in_dim=latent_space_dim+1, last_layer=last_layer)
                else:
                    raise ValueError('Image width {} not handled for 2D images.'.format(image_width))
            elif self.data_dim == 3:
                if image_width == 64:
                    self.encoder = RCNN3D64(hidden_dim=encoder_hidden_dim, out_dim=self.encoder_dim)
                    self.decoder = Deconv3D64(in_dim=latent_space_dim+1, last_layer=last_layer)
                elif image_width == 128:
                    self.encoder = RCNN3D128(hidden_dim=encoder_hidden_dim, out_dim=self.encoder_dim)
                    self.decoder = Deconv3D128(in_dim=latent_space_dim+1, last_layer=last_layer)
                else:
                    raise ValueError('Image width {} not handled for 3D images.'.format(image_width))

        if self.use_cuda:
            print('Setting nets to cuda')
            self.encoder.cuda()
            self.decoder.cuda()

        else:
            self.encoder.float()
            self.decoder.float()

    def parameters(self):
        return list(self.encoder.parameters()) + list(self.decoder.parameters())

    def decode(self, latent_position_batch, times_batch, first_val_batch):
        to_feed = torch.cat([latent_position_batch, times_batch], 1)
        return self.decoder(to_feed) #+ first_val_batch

    def get_reconstructions(self, dataloader):
        n_to_plot = 4
        reconstructed_list, real_list, times_list, latent_positions_list = [], [], [], []

        for i in range(n_to_plot):
            data = next(iter(dataloader))
            times = data['times'].squeeze(0)
            values = data['values'].squeeze(0)

            # We need to feed the sequence to the neural network
            latent_position = self.encoder.get_sequence_output(times, values)
            latent_position = latent_position.unsqueeze(0).expand(len(times), self.latent_space_dim)

            real_list.append(values)
            reconstructed_list.append(self.decode(latent_position, times, values[0].unsqueeze(0).expand((len(times),) + self.value_shape)))
            times_list.append(times.cpu().detach().numpy())

        return times_list, real_list, reconstructed_list

    def plot_reconstructions(self, times_list, real_list, reconstructed_list, output_dir):

        if not self.is_image:
            plt.figure(figsize=(6, 6))
            plt.clf()
            # Plotting the reconstructions
            f, axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(12, 12))
            for k, ax in enumerate(axes.flatten()):
                for i in range(self.data_dim):
                    ax.plot(times_list[k], real_list[k].cpu().detach().numpy()[:, i], c=self.colors[i], label=self.labels[i], linestyle='dotted')
                    ax.plot(times_list[k], reconstructed_list[k].cpu().detach().numpy()[:, i], c=self.colors[i])
                    ax.legend()

            plt.legend()
            plt.savefig(os.path.join(output_dir, 'reconstructions.pdf'))
            plt.figure(figsize=(6, 6))

        else:
            for k, (times, real, reconstructed) in enumerate(zip(times_list, real_list, reconstructed_list)):
                if self.data_dim == 2:
                    to_save = torch.cat((real, reconstructed), 0)
                else:
                    # We extract and save some slices...
                    slice_no = 32
                    to_save = torch.cat((real[:, :, 32, :, :], reconstructed[:, :, 32, :, :],
                                         real[:, :, :, 32, :], reconstructed[:, :, :, 32, :],
                                         real[:, :, :, :, 32], reconstructed[:, :, :, :, 32]))
                save_image(to_save, os.path.join(output_dir, 'target_and_reconstruction_{}.pdf'.format(k)),
                           nrow=len(real), normalize=True)

    def get_mean_trajectory(self, dataset):
        if self.is_image:
            times_np = np.linspace(dataset.min_time - 1., dataset.max_time + 1., 10)
        else:
            times_np = np.linspace(dataset.min_time - 1., dataset.max_time + 1., 30)
        times_torch = torch.from_numpy(times_np).type(dataset.type).unsqueeze(1)

        latent_pos_np = np.zeros((len(times_np), self.latent_space_dim))
        latent_pos = torch.from_numpy(latent_pos_np).type(dataset.type)

        input = torch.cat((latent_pos, times_torch), 1)

        traj = self.decoder(input)

        return times_torch, traj

    def plot_average_trajectory(self, output_dir, dataset, latent_positions=None):
        times, trajectory = self.get_mean_trajectory(dataset, latent_positions=latent_positions)
        times = times.cpu().detach().numpy()

        if not self.is_image:
            plt.figure(figsize=(6, 6))
            plt.clf()
            for i in range(self.data_dim):
                if self.labels is not None:
                    plt.plot(times, trajectory.cpu().detach().numpy()[:, i], label=self.labels[i], c=self.colors[i])
                else:
                    plt.plot(times, trajectory.cpu().detach().numpy()[:, i], c=self.colors[i])
            plt.xlabel('Time')
            plt.legend()
            plt.savefig(os.path.join(output_dir, 'average_trajectory.pdf'))

        else:
            if self.data_dim == 3:
                trajectory = torch.cat((trajectory[:, :, 32, :, :], trajectory[:, :, :, 32, :], trajectory[:, :, :, :, 32]))
            save_image(trajectory, os.path.join(output_dir, 'average_trajectory.pdf'), nrow=10, normalize=True)

        del trajectory

    def save(self, output_dir):
        model_save_dir = os.path.join(output_dir, 'model')
        if not os.path.isdir(model_save_dir):
            os.mkdir(model_save_dir)

        pickle.dump((self.data_dim, self.latent_space_dim, self.is_image, self.labels,
                     self.use_cuda, self.encoder_dim, self.encoder_hidden_dim,
                     self.decoder_hidden_dim, self.image_width, self.colors),
                     open(os.path.join(model_save_dir, 'model_info.p'), 'wb'))

        torch.save(self.encoder.state_dict(), os.path.join(model_save_dir, 'encoder.p'))
        torch.save(self.decoder.state_dict(), os.path.join(model_save_dir, 'decoder.p'))

    def compute_residuals(self, dataloader):
        """
        Encode and decode each subject, and save the mean squared error.
        """
        residuals = []
        ids = []

        latent_positions = []

        for data in dataloader:
            times = data['times'].squeeze(0)
            values = data['values'].squeeze(0)
            latent_position = self.encoder.get_sequence_output(times, values)
            latent_positions.append(latent_position.detach().cpu().numpy())

            latent_position = latent_position.unsqueeze(0).expand(len(times), self.latent_space_dim)
            reconstructed = self.decode(latent_position, times, values[0].unsqueeze(0).expand((len(times),) + self.value_shape))

            errors = ((reconstructed - values) ** 2).view(len(times), -1).cpu().detach().numpy()
            errors = np.mean(errors, axis=1)
            assert len(errors) == len(times)
            for error in errors:
                residuals.append(error)
                ids.append(data['idx'].squeeze(0).cpu().detach().numpy())

        residuals = np.array(residuals)

        return ids, residuals, np.array(latent_positions)

    def extra_writing(self, output_dir, dataloader, dico):
        pass


def load_model(folder):
    data_dim, latent_space_dim, is_image, labels, use_cuda, encoder_dim, encoder_hidden_dim, \
        decoder_hidden_dim, image_width, colors = pickle.load(open(os.path.join(folder, 'model_info.p'), 'rb'))

    model = LongitudinalAutoencoder(data_dim, latent_space_dim=latent_space_dim, is_image=is_image, encoder_hidden_dim=encoder_hidden_dim,
                             decoder_hidden_dim=decoder_hidden_dim,
                             labels=labels, use_cuda=False, image_width=image_width)

    model.encoder.load_state_dict(torch.load(os.path.join(folder, 'encoder.p'), map_location=lambda storage, loc: storage))
    model.decoder.load_state_dict(torch.load(os.path.join(folder, 'decoder.p'), map_location=lambda storage, loc: storage))

    return model

