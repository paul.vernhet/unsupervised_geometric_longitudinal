import os
from joblib import Parallel, delayed
from Benchmark import Benchmark




#%% Launch

name = 'experiment_2'

output_dir = '../output/benchmark_multimodal_{0}/'.format(name)
project_data_path = '../data/data_pet'
atlas_path = os.path.join(project_data_path, 'atlas','AAL2.nii')
pet_path = os.path.join(project_data_path, 'df_aal2.csv')
adas_path = '../data/data_adas/df.csv'
paths = {'pet_scores': pet_path, 'adas_scores': adas_path, 'output_dir': output_dir, 'atlas': atlas_path}
pre_encoder_dim = 12
pre_decoder_dim = 12
latent_space_dim = 6
random_slope = False
n_epochs = 2
n_jobs = 2
n_runs = 60
validation = (6, 10)

nn_parameters = latent_space_dim, pre_encoder_dim, pre_decoder_dim, random_slope, n_epochs

experiment = Benchmark(paths, nn_parameters, seed=0, validation=validation, name=name)

def compute_experiment_1_fold(experiment, fold):
    return experiment.compute_fold(fold)

Parallel(n_jobs=n_jobs)(delayed(compute_experiment_1_fold)(experiment, i) for i in range(n_runs))


