import os
from longitudinal_scalar_dataset import LongitudinalScalarDataset
from random_slope_model import RandomSlopeModel
from estimate_adversarial import estimate_random_slope_model_adversarial
import numpy as np
from samplers import BiSampler, NormalSampler, UniformSampler
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from torch.utils.data import DataLoader
import argparse
import pandas as pd


def save_dataset_info(dataset, model, prefix, output_dir):
    dataloader = DataLoader(dataset)
    ids, residuals, latent_positions = model.compute_residuals(dataloader)
    np.savetxt(os.path.join(output_dir, prefix + '_residuals.txt'), residuals)
    np.savetxt(os.path.join(output_dir, prefix + '_ids.txt'), ids)
    np.savetxt(os.path.join(output_dir, prefix + '_latent_positions.txt'), latent_positions)


def extract_id_from_name(name):
    return float(name[-4:])


def launch_simulation(output_dir, train_dataset, test_dataset, randomize_nb_observations=False, sampler_type='normal', l=10.):
    print('Output directory', output_dir)
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    train_dataset.save(os.path.join(output_dir, 'train_dataset.p'))
    test_dataset.save(os.path.join(output_dir, 'test_dataset.p'))

    random_slope_model = RandomSlopeModel(
        data_dim=4,
        latent_space_dim=latent_space_dim,
        is_image=False,
        encoder_hidden_dim=64,
        decoder_hidden_dim=64,
        labels=['memory', 'language', 'praxis', 'concentration'],
        initial_slope_norm=4.5,
        random_slope=random_slope
    )

    if sampler_type == 'normal':
        sampler = NormalSampler(dimension=random_slope_model.encoder_dim)
    elif sampler_type == 'uniform':
        sampler = UniformSampler(dimension=random_slope_model.encoder_dim)

    def call_back(model):
        save_dataset_info(train_dataset, model, 'train', output_dir)
        save_dataset_info(test_dataset, model, 'test', output_dir)

    estimate_random_slope_model_adversarial(random_slope_model, train_dataset, n_epochs=10000,
                                            learning_rate=1e-4, output_dir=output_dir,
                                            batch_size=32, save_every_n_iters=100, sampler=sampler,
                                            call_back=call_back, lr_decay=0.99,
                                            randomize_nb_observations=randomize_nb_observations, l=l)

# Parsing some command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("-fold", "-fold", default=0)
parser.add_argument("-l","-l", default=10.)
args = parser.parse_args()
fold_number = int(args.fold)
l = float(args.l)

print('Value of l used {}'.format(l))

# Loading the data
df = pd.read_csv('../data_adas/df.csv')
print(df.head())
ids = df.values[:, 0]
ids = [extract_id_from_name(elt) for elt in ids]
values = df.values[:, 2:6]
times = df.values[:, 1]


latent_space_dim = 2
random_slope = False

labels = {}
for rid in ids:
    labels[rid] = 0

distinct_rids = np.array(list(set(ids)))

# Attempt with uniform sampler:
# kf = KFold(n_splits=10, shuffle=True, random_state=42)
# kf.get_n_splits(distinct_rids)
#
# for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
#     if fold == fold_number:
#         train_ids_unique = distinct_rids[train_index]
#         test_ids_unique = distinct_rids[test_index]
#
#         train_ids, train_values, train_times = [], [], []
#         test_ids, test_values, test_times = [], [], []
#         for (rid, value, time) in zip(ids, values, times):
#             if rid in train_ids_unique:
#                 train_ids.append(rid)
#                 train_values.append(value)
#                 train_times.append(time)
#
#             else:
#                 assert rid in test_ids_unique
#                 test_ids.append(rid)
#                 test_values.append(value)
#                 test_times.append(time)
#
#         output_dir = '../output_adas/output_uniform_sampler_fold_{}'.format(fold)
#
#         train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times)
#         test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
#                                                  ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)
#
#         launch_simulation(output_dir, train_dataset, test_dataset,
#                           randomize_nb_observations=False, sampler_type='uniform')


# #10-fold with the randomized number of observations.
# kf = KFold(n_splits=10, shuffle=True, random_state=42)
# kf.get_n_splits(distinct_rids)
#
# for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
#     if fold == fold_number:
#         train_ids_unique = distinct_rids[train_index]
#         test_ids_unique = distinct_rids[test_index]
#
#         train_ids, train_values, train_times = [], [], []
#         test_ids, test_values, test_times = [], [], []
#         for (rid, value, time) in zip(ids, values, times):
#             if rid in train_ids_unique:
#                 train_ids.append(rid)
#                 train_values.append(value)
#                 train_times.append(time)
#
#             else:
#                 assert rid in test_ids_unique
#                 test_ids.append(rid)
#                 test_values.append(value)
#                 test_times.append(time)
#
#         output_dir = '../output_adas/output_randomized_nb_obs_fold_{}'.format(fold)
#
#         train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times)
#         test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
#                                                  ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)
#
#         launch_simulation(output_dir, train_dataset, test_dataset, randomize_nb_observations=True)

# # EXPERIMENTS WITH VARYING TRAIN SET SIZE.
# for test_size in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]:
#     train_ids_unique, test_ids_unique, _, _ = train_test_split(distinct_rids, distinct_rids, test_size=test_size)
#
#     train_ids, train_values, train_times = [], [], []
#     test_ids, test_values, test_times = [], [], []
#     for (rid, value, time) in zip(ids, values, times):
#         if rid in train_ids_unique:
#             train_ids.append(rid)
#             train_values.append(value)
#             train_times.append(time)
#
#         else:
#             assert rid in test_ids_unique
#             test_ids.append(rid)
#             test_values.append(value)
#             test_times.append(time)
#
#     output_dir = '../output_adas/output_test_{}_fold_{}'.format(test_size, fold_number)
#
#     train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times)
#     test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
#                                              ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)
#
#     launch_simulation(output_dir, train_dataset, test_dataset)



#10-fold on the cognitive scores
# kf = KFold(n_splits=10, shuffle=True, random_state=42)
# kf.get_n_splits(distinct_rids)
#
# for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
#     if fold == fold_number:
#         train_ids_unique = distinct_rids[train_index]
#         test_ids_unique = distinct_rids[test_index]
#
#         train_ids, train_values, train_times = [], [], []
#         test_ids, test_values, test_times = [], [], []
#         for (rid, value, time) in zip(ids, values, times):
#             if rid in train_ids_unique:
#                 train_ids.append(rid)
#                 train_values.append(value)
#                 train_times.append(time)
#
#             else:
#                 assert rid in test_ids_unique
#                 test_ids.append(rid)
#                 test_values.append(value)
#                 test_times.append(time)
#
#         output_dir = '../output_adas/output_fold_{}'.format(fold)
#
#         train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times)
#         test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
#                                                  ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)
#
#         launch_simulation(output_dir, train_dataset, test_dataset)

#10-fold on the cognitive scores
kf = KFold(n_splits=10, shuffle=True, random_state=42)
kf.get_n_splits(distinct_rids)

for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
    if fold == fold_number:
        train_ids_unique = distinct_rids[train_index]
        test_ids_unique = distinct_rids[test_index]

        train_ids, train_values, train_times = [], [], []
        test_ids, test_values, test_times = [], [], []
        for (rid, value, time) in zip(ids, values, times):
            if rid in train_ids_unique:
                train_ids.append(rid)
                train_values.append(value)
                train_times.append(time)

            else:
                assert rid in test_ids_unique
                test_ids.append(rid)
                test_values.append(value)
                test_times.append(time)

        output_dir = '../output_adas/output_l_{}_fold_{}'.format(l, fold)

        train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times)
        test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
                                                 ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)

        launch_simulation(output_dir, train_dataset, test_dataset, l=l)



# 10-fold on the cognitive scores
# kf = KFold(n_splits=10, shuffle=True, random_state=42)
# kf.get_n_splits(distinct_rids)
#
# for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
#     if fold == fold_number:
#         train_ids_unique = distinct_rids[train_index]
#         test_ids_unique = distinct_rids[test_index]
#
#         train_ids, train_values, train_times = [], [], []
#         test_ids, test_values, test_times = [], [], []
#         for (rid, value, time) in zip(ids, values, times):
#             if rid in train_ids_unique:
#                 train_ids.append(rid)
#                 train_values.append(value)
#                 train_times.append(time)
#
#             else:
#                 assert rid in test_ids_unique
#                 test_ids.append(rid)
#                 test_values.append(value)
#                 test_times.append(time)
#
#         output_dir = '../output_adas/output_l_{}'.format(l)
#
#         train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times)
#         test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
#                                                  ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)
#
#         launch_simulation(output_dir, train_dataset, test_dataset, l=l)


# Exoeriments with varying number of observations
# nb_obs_to_keep = [2, 3, 4, 5, 6]
# kf = KFold(n_splits=10, shuffle=True, random_state=42)
# kf.get_n_splits(distinct_rids)
#
# for nb_obs in nb_obs_to_keep:
#     for fold, (train_index, test_index) in enumerate(kf.split(distinct_rids)):
#         if fold == fold_number:
#             train_ids_unique = distinct_rids[train_index]
#             test_ids_unique = distinct_rids[test_index]
#
#             train_ids, train_values, train_times = [], [], []
#             test_ids, test_values, test_times = [], [], []
#             for (rid, value, time) in zip(ids, values, times):
#                 if rid in train_ids_unique:
#                     train_ids.append(rid)
#                     train_values.append(value)
#                     train_times.append(time)
#
#                 else:
#                     assert rid in test_ids_unique
#                     test_ids.append(rid)
#                     test_values.append(value)
#                     test_times.append(time)
#
#             output_dir = '../output_adas/output_nb_obs_{}_fold_{}'.format(nb_obs, fold)
#
#             train_dataset = LongitudinalScalarDataset(train_ids, train_values, train_times)
#             train_dataset.keep_only_n_observations_per_subject(nb_obs)
#
#             test_dataset = LongitudinalScalarDataset(test_ids, test_values, test_times,
#                                                      ages_std=train_dataset.ages_std, ages_mean=train_dataset.ages_mean)
#
#             launch_simulation(output_dir, train_dataset, test_dataset)
