import os
import numpy as np
import torch.nn as nn
import torch
from networks import ScalarRNN, Decoder, RCNN64, RCNN3D64, Deconv64, \
    Deconv3D64, RCNN128, RCNN3D128, PreDecoder, MergeNetwork, RCNN3D32, Deconv3D32
import pickle
from torchvision.utils import save_image
from utils import reparametrize
from torch.distributions.normal import Normal
from multimodal_dataset import DatasetTypes
import nibabel as nib

import platform
from utils import get_pet_slices

if platform.system() == 'Linux':
    import matplotlib as matplotlib

    matplotlib.use('agg')

import matplotlib.pyplot as plt

from PIL import Image


class RandomSlopeModel:

    def __init__(self, data_info, latent_space_dim, pre_encoder_dim=None,# pre_decoder_dim=None,
                 use_cuda=False, random_slope=False, variational=False, atlas_path=None, depth="normal"):
        """
        Constructor
        :param data_info: info about the used modalities:
        :param latent_space_dim: Dimension of the space of the trajectories
        :param pre_encoder_dim: Dimension output by each individual encoder
        :param pre_decoder_dim: Dimension output before decoding, common for all modalities
        :param use_cuda: Whether to use cuda for computations (if available)
        :param random_slope: Whether the slopes are also random
        :param variational: Whether to use a variational cost for estimation
        :param atlas_path: Atlas used to save pet images.
        """
        self.data_info = data_info
        self.latent_space_dim = latent_space_dim
        self.use_cuda = use_cuda
        self.random_slope = random_slope
        self.colors = ['r', 'g', 'y', 'b', 'k']
        self.variational = variational
        self.type = torch.cuda.FloatTensor if use_cuda else torch.FloatTensor

        # The mean trajectory in latent space is t -> e_1 * t
        mean_slope_np = np.zeros(latent_space_dim)
        mean_slope_np[0] = 1.
        self.mean_slope = torch.from_numpy(mean_slope_np).float()

        # Dimension of the variables describing the trajectories
        self.encoder_dim = self.latent_space_dim + 1 if not self.random_slope else 2*self.latent_space_dim

        self.prior_parameters = {
            'eta_mean': (4 * torch.zeros(1)),
            'time_shift_mean': torch.zeros(1),
            'eta_log_variance': torch.zeros(1),
            'time_shift_log_variance': (torch.zeros(1)),
            'others_mean': torch.zeros(self.latent_space_dim - 1),
            'others_log_variance': torch.zeros(self.latent_space_dim - 1)
        }

        if len(data_info) == 1:
            self.monomodal = True
            self.pre_encoder_dim = self.encoder_dim
            #self.pre_decoder_dim = self.latent_space_dim

        else:
            self.monomodal = False
            #assert pre_encoder_dim != None, 'Please specify pre_encoder_dim in the multimodal case'
            #assert pre_decoder_dim != None, 'Please specidy pre_decoder_dim in the multimodal case'
            self.pre_encoder_dim = pre_encoder_dim
            #self.pre_decoder_dim = pre_decoder_dim


        self.depth = depth
        self.encoders = {}
        self.decoders = {}
        self.initialize_encoder_and_decoder()

        self.update_types()

        # Atlas for PET scans
        if atlas_path is not None:
            self.atlas_input_image = None
            self.atlas_labels = None
            self.indices = None
            self._initialize_atlas(atlas_path)

        # Regularization
        self.dropout = False

    def _initialize_atlas(self, atlas_path):
        """
        Initializes atlas attribute, used to plot PET reconstructions
        :param atlas_path:
        :return:
        """
        input_image = nib.load(atlas_path)
        image = input_image.get_data()
        image = np.array(image, dtype='f')

        self.atlas_input_image = input_image
        self.atlas_labels = list(set(image.ravel()))
        self.indices = {}

        for index, n in enumerate(self.atlas_labels):
            self.indices[n] = np.array(np.where(image == n))

    def update_types(self):
        """
        Handles cuda and non-cuda types for all the tensors and neural networks attributes.
        """
        for key in self.encoders.keys():
            self.encoders[key].float()
            self.decoders[key].float()
            if self.use_cuda:
                self.encoders[key].cuda()
                self.decoders[key].cuda()

        for key in self.prior_parameters:
            self.prior_parameters[key] = self.prior_parameters[key].type(self.type).requires_grad_(True)

        self.mean_slope = self.mean_slope.type(self.type)

        if not self.monomodal:
            # self.fc_merge.float()
            self.merge_network.float()
           #self.pre_decoder.float()
            if self.use_cuda:
                self.merge_network = self.merge_network.cuda()
                #self.pre_decoder = self.pre_decoder.cuda()


        if self.variational:
            self.fc_log_variances.float()
            if self.use_cuda:
                self.fc_log_variances.cuda()

        print(self.prior_parameters)

    def initialize_encoder_and_decoder(self):
        """
        Reads the info for the data at hand and initialize the needed encoders/decoders
        """
        for dataset_name, (dataset_type, encoder_hidden_dim, decoder_hidden_dim, data_dim, labels, colors) in self.data_info.items():
            if dataset_type in [DatasetTypes.SCALAR, DatasetTypes.PET]:
                self.encoders[dataset_name] = ScalarRNN(in_dim=data_dim+1,
                                                        hidden_dim=encoder_hidden_dim,
                                                        out_dim=self.pre_encoder_dim)
                self.decoders[dataset_name] = Decoder(in_dim=self.latent_space_dim, out_dim=data_dim,
                                                      hidden_dim=decoder_hidden_dim)

            elif dataset_type == DatasetTypes.IMAGE:
                if data_dim == (64, 64):
                    self.encoders[dataset_name] = RCNN64(hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    #self.decoders[dataset_name] = Deconv64(in_dim=self.pre_decoder_dim)
                    self.decoders[dataset_name] = Deconv64(in_dim=self.latent_space_dim)
                elif data_dim == (64, 64, 64):
                    self.encoders[dataset_name] = RCNN3D64(hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    #self.decoders[dataset_name] = Deconv3D64(in_dim=self.pre_decoder_dim)
                    self.decoders[dataset_name] = Deconv3D64(in_dim=self.latent_space_dim)
                elif data_dim == (32, 32, 32):
                    self.encoders[dataset_name] = RCNN3D32(hidden_dim=encoder_hidden_dim, out_dim=self.pre_encoder_dim)
                    #self.decoders[dataset_name] = Deconv3D64(in_dim=self.pre_decoder_dim)
                    self.decoders[dataset_name] = Deconv3D32(in_dim=self.latent_space_dim)
                else:
                    raise ValueError('Unexpected observation shape')

            else:
                raise ValueError('Unexpected dataset type')

        if not self.monomodal:
            assert not self.variational, 'Variational not compatible with new merge op'
            self.merge_network = MergeNetwork(in_dim=len(self.data_info) * self.pre_encoder_dim, hidden_dim=8,
                                              out_dim=self.encoder_dim, depth=self.depth)
            #self.pre_decoder = PreDecoder(in_dim=self.latent_space_dim, hidden_dim=self.pre_decoder_dim,
            #                              out_dim=self.pre_decoder_dim, depth=self.depth)

        if self.variational:
            self.fc_log_variances = nn.Linear(self.pre_encoder_dim, self.encoder_dim)

    def sample_to_mean_and_slope(self, sample):
        """
        Goes from latent variable z to slope and intercept.
        """
        alpha, tau, source = torch.exp(sample[-1]), sample[0], sample[:-1]
        slope = alpha * self.mean_slope
        intercept = source - (1 + alpha) * tau * self.mean_slope
        return intercept, slope

    def get_latent_trajectories(self, data, mean, log_variances=None, sample=False):
        """
        Outputs the latent trajectories t -> alpha_i (t -tau_i) *e_0 + sources, computed at the times required for each
        modality
        """
        out = {}
        for (key, val) in data.items():
            if key != 'idx':
                out[key] = self.get_latent_positions_sample_from_means_and_log_variances(mean,
                                                                                     val['times'].squeeze(0),
                                                                                     log_variances=log_variances,
                                                                                     sample=sample)
        return out

    def get_latent_positions_sample_from_means_and_log_variances(self, mean, times, log_variances=None, sample=False):
        """
        Goes from latent variable z all the way to the latent trajectories for each modality.
        """
        if sample:
            sampled_mean = reparametrize(mean, log_variances)
        else:
            sampled_mean = mean

        intercept, slope = self.sample_to_mean_and_slope(sampled_mean)

        return intercept.view(1, -1).expand(len(times), self.latent_space_dim) + \
               times.view(-1, 1).expand(len(times), self.latent_space_dim) * \
               slope.view(1, -1).expand(len(times), self.latent_space_dim)

    def parameters(self):
        """
        All the parameters to be optimized.
        """
        out = []
        for key in self.encoders.keys():
            out = out + list(self.encoders[key].parameters()) + list(self.decoders[key].parameters())
        if not self.monomodal:
            out = out + list(self.merge_network.parameters())
            # out = out + list(self.fc_merge.parameters())
            #out = out + list(self.pre_decoder.parameters())
        if self.variational:
            out = out + list(self.fc_log_variances.parameters())
        return out #+ [self.prior_parameters['time_shift_mean']] + [self.prior_parameters['eta_mean']] # [self.prior_parameters['eta_log_variance']] #+ \

               #[self.prior_parameters['time_shift_mean']] +  #+ \
               #[self.prior_parameters['time_shift_log_variance']] #+ [self.prior_parameters['eta_log_variance']]

    def decode(self, latent_trajectories):
        """
        :param latent_trajectories: dictionnary of latent positions for each modality
        :return: a dictionnary of decoded values.
        """
        out = {}
        for key in latent_trajectories.keys():
            if len(latent_trajectories[key]) > 0:
                if not self.monomodal:
                    #out[key] = self.decoders[key](self.pre_decoder(latent_trajectories[key]))
                    out[key] = self.decoders[key](latent_trajectories[key])
                else:
                    out[key] = self.decoders[key](latent_trajectories[key])
        return out

    def encode(self, data, randomize_nb_obs=False):
        """
        :param data: dictionnary. Each entry is a dictionary which contains times and observations
        :return: a latent variable z.
        """
        encoded = torch.from_numpy(np.zeros((len(self.data_info), self.pre_encoder_dim))).type(self.type)
        i = 0
        for key in self.data_info.keys():
            if key in data.keys():
                times = data[key]['times'].squeeze(0)
                values = data[key]['values'].squeeze(0)

                if randomize_nb_obs and len(times) > 2:
                    n_to_keep = np.random.randint(2, len(times))
                    indices_to_keep = np.sort(np.random.choice(range(len(times)), n_to_keep, replace=False))
                    times = times[indices_to_keep]
                    values = values[indices_to_keep]

                encoded[i] = self.encoders[key].get_sequence_output(times, values)
            i += 1

        # Merging the representations:
        return self._merge_representation(encoded)

    def _merge_representation(self, codes):
        if self.monomodal:  # single modality: do nothing
            if self.variational:
                return codes.squeeze(0), self.fc_log_variances(torch.tanh(codes.squeeze(0)))  # TODO: not ideal here, the variational is too simple (but not used anymore)
            else:
                return codes.squeeze(0)
        else:
            return self.merge_network(codes.view(-1))
            # Alternative versions can be used BUT parameters() need to be modified subsequently
            # mean_representation = torch.mean(codes, 0)
            # if self.variational:
            #     return self.fc_merge(torch.tanh(mean_representation)), self.fc_log_variances(torch.tanh(mean_representation))
            # else:
            #     if self.dropout:
            #         dropout = nn.Dropout(p=0.2)
            #         return dropout(self.fc_merge(torch.tanh(mean_representation)))
            #     else:
            #         return self.fc_merge(torch.tanh(mean_representation))

    def set_prior_parameters(self, d):
        for (key, val) in d.items():
            self.prior_parameters[key] = torch.from_numpy(val).float()
            if self.use_cuda:
                self.prior_parameters[key] = self.prior_parameters[key].cuda()

    def get_prior_log_variance(self):
        return torch.cat([self.prior_parameters['time_shift_log_variance'],
                          self.prior_parameters['others_log_variance'],
                          self.prior_parameters['eta_log_variance']], 0)

    def get_prior_mean(self):
        return torch.cat([self.prior_parameters['time_shift_mean'],
                          self.prior_parameters['others_mean'],
                          self.prior_parameters['eta_mean']],
                         0)

    def compute_regularity(self, means_batch, log_variances_batch=None):
        if self.variational:
            return self._compute_variational_regularity(means_batch, log_variances_batch)
        else:
            assert log_variances_batch is None, 'oops'
            return self._compute_simple_regularity(means_batch)

    def _compute_simple_regularity(self, means_batch):
        mean_prior = self.get_prior_mean()
        std_model = torch.sqrt(torch.exp(self.get_prior_log_variance()))
        distribution = Normal(loc=mean_prior, scale=std_model)
        return -1. * torch.sum(distribution.log_prob(means_batch))

    def _compute_variational_regularity(self, means_batch, log_variances_batch):
        log_variances_model = self.get_prior_log_variance().view(1, -1).expand(log_variances_batch.size())
        means_model = self.get_prior_mean().view(1, -1).expand(means_batch.size())

        return 0.5 * torch.sum(torch.exp(log_variances_batch) / torch.exp(log_variances_model)
                               + torch.exp(log_variances_model) * (means_model - means_batch) ** 2
                               - 1
                               + log_variances_model - log_variances_batch)

    def get_reconstructions(self, dataloader):
        n_to_plot = 4
        reconstructed, real, times = {}, {}, {}
        for key in self.data_info.keys():
            reconstructed[key] = []
            real[key] = []
            times[key] = []

        for i in range(n_to_plot):
            data = next(iter(dataloader))

            if self.variational:
                mean, _ = self.encode(data)
            else:
                mean = self.encode(data)

            latent_traj = self.get_latent_trajectories(data, mean, log_variances=None, sample=False)
            reconst = self.decode(latent_traj)

            for key in reconst.keys():
                reconstructed[key].append(reconst[key].cpu().detach())
                real[key].append(data[key]['values'].squeeze(0).cpu().detach())
                times[key].append(data[key]['times'].squeeze(0).cpu().detach())

        return times, real, reconstructed

    def _load_pet(self, signal):

        # Load pet data
        image = np.array(self.atlas_input_image.get_data(), dtype='f')
        output_image_p_value =image.copy()

        for index, n in enumerate(self.atlas_labels):
            #indice = np.array(np.where(image == n))
            indice = self.indices[n]

            if index == 0:
                valore = np.NaN
            else:
                valore = signal[index - 1]

            output_image_p_value[indice[0, :], indice[1, :], indice[2, :]] = valore

        output_pet = nib.Nifti1Image(output_image_p_value, self.atlas_input_image.get_affine())
        output_numpy = np.array(output_pet.get_data(), dtype='f')
        return output_numpy

    def plot_reconstructions(self, times, real, reconstructed, output_dir, split="train"):

        for key in times.keys():
            dataset_type, _, _, data_dim, labels, colors = self.data_info[key]

            # Scalar dataset case
            if dataset_type == DatasetTypes.SCALAR:
                plt.figure(figsize=(6, 6))
                plt.clf()
                # Plotting the reconstructions
                f, axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(12, 12))
                for k, ax in enumerate(axes.flatten()):
                    if k < len(real[key]):
                        for i in range(data_dim):
                            ax.plot(times[key][k].cpu().detach().numpy(),
                                    real[key][k].cpu().detach().numpy()[:, i],
                                    c=colors[i], label=labels[i], linestyle='dotted')
                            ax.plot(times[key][k].cpu().detach().numpy(),
                                    reconstructed[key][k].cpu().detach().numpy()[:, i],
                                    c=colors[i])
                            ax.legend()

                plt.legend()
                plt.savefig(os.path.join(output_dir, key + '_reconstructions_{0}.pdf'.format(split)))
                plt.figure(figsize=(6, 6))

            elif dataset_type == DatasetTypes.PET:

                plt.figure(figsize=(6, 6))
                plt.clf()
                # Plotting the reconstructions
                f, axes = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(12, 12))
                for k, ax in enumerate(axes.flatten()):
                    if k < len(times[key]):
                        for i in range(min(data_dim,10)):
                            ax.plot(times[key][k].cpu().detach().numpy(),
                                    real[key][k].cpu().detach().numpy()[:, i],
                                    c=colors[i], label=labels[i], linestyle='dotted')
                            ax.plot(times[key][k].cpu().detach().numpy(),
                                    reconstructed[key][k].cpu().detach().numpy()[:, i],
                                    c=colors[i])
                            ax.legend()

                plt.legend()
                plt.savefig(os.path.join(output_dir, key + '_reconstructions_{0}.pdf'.format(split)))
                plt.close()
                plt.figure(figsize=(6, 6))

                # Plot PET as images
                for k, (t, reals, reconsts) in enumerate(zip(times[key], real[key], reconstructed[key])):
                    # Get reals and reconsts
                    reals_concatenated = [self._load_pet(real) for real in reals]
                    reconsts_concatenated = [self._load_pet(reconst) for reconst in reconsts]

                    reals_concatenated_0, reals_concatenated_1, reals_concatenated_2 = get_pet_slices(reals_concatenated)
                    reconsts_concatenated_0, reconsts_concatenated_1, reconsts_concatenated_2 = get_pet_slices(reconsts_concatenated)

                    res = np.concatenate([reals_concatenated_0, reconsts_concatenated_0,
                                          reals_concatenated_1, reconsts_concatenated_1,
                                          reals_concatenated_2, reconsts_concatenated_2], axis = 0)

                    res = (res-np.nanmean(res))/np.nanstd(res)

                    plt.clf()
                    plt.imshow(res, cmap=plt.cm.YlOrBr)
                    plt.axis('off')
                    plt.savefig(os.path.join(output_dir, key + '_np_pet_target_and_reconstruction_{0}_{1}.pdf'.format(k, split)))
                    plt.close()


            # Image case
            else:
                for k, (t, reals, reconst) in enumerate(zip(times[key], real[key], reconstructed[key])):
                    if data_dim == (64, 64):
                        to_save = torch.cat((reals, reconst), 0)
                    elif data_dim == (64, 64, 64):
                        # We extract and save some slices...
                        to_save = torch.cat((reals[:, :, 25, :, :], reconst[:, :, 25, :, :],
                                             reals[:, :, :, 32, :], reconst[:, :, :, 32, :],
                                             reals[:, :, :, :, 32], reconst[:, :, :, :, 32]))
                    elif data_dim == (32, 32, 32):
                        # We extract and save some slices...
                        to_save = torch.cat((reals[:, :, 16, :, :], reconst[:, :, 16, :, :],
                                             reals[:, :, :, 16, :], reconst[:, :, :, 16, :],
                                             reals[:, :, :, :, 16], reconst[:, :, :, :, 16]))
                    else:
                        raise ValueError('Unexpected image shape')

                    save_image(to_save, os.path.join(output_dir, key + '_target_and_reconstruction_{0}_{1}.pdf'.format(k, split)),
                               nrow=len(reals), normalize=True)

    def get_mean_trajectory(self, min_x, max_x):
        """
        :param min_x: min x coordinate in latent space, from which to plot
        :param max_x: max x coordinate in latent space, until which to plot
        :return: two dictionaries: the x coordinates and the values for each modality label
        """
        x_out = {}
        mean_traj = {}
        for key, decoder in self.decoders.items():
            dataset_type, _, _, _, _, _ = self.data_info[key]
            nb_points = 10 if dataset_type in [DatasetTypes.IMAGE, DatasetTypes.PET] else 50
            x_np = np.linspace(min_x, max_x, nb_points)
            x_torch = torch.from_numpy(x_np).type(self.type)
            latent_traj_np = np.array([np.zeros(self.latent_space_dim) + self.mean_slope.cpu().detach().numpy() * t for t in x_np])
            latent_traj = torch.from_numpy(latent_traj_np).type(self.type)
            x_out[key] = x_torch
            if self.monomodal:
                mean_traj[key] = decoder(latent_traj)
            else:
                #mean_traj[key] = decoder(self.pre_decoder(latent_traj))
                mean_traj[key] = decoder(latent_traj)
        return x_out, mean_traj

    def plot_average_trajectory(self, output_dir, min_x=None, max_x=None):
        x_out, mean_traj = self.get_mean_trajectory(min_x=min_x, max_x=max_x)

        plt.figure(figsize=(6, 6))
        plt.clf()

        for key in x_out.keys():
            times = x_out[key].cpu().detach().numpy()

            # handle cases here
            dataset_type, _, _, data_dim, labels, colors = self.data_info[key]

            if dataset_type == DatasetTypes.SCALAR:
                plt.clf()
                for i in range(data_dim):
                    if labels is not None:
                        plt.plot(times, mean_traj[key].cpu().detach().numpy()[:, i], label=labels[i], c=colors[i])
                    else:
                        plt.plot(times, mean_traj[key].cpu().detach().numpy()[:, i], c=colors[i])
                plt.xlabel('x')
                plt.legend()
                plt.savefig(os.path.join(output_dir, key+'_scalar_average_trajectory.pdf'))
                plt.close()

            elif dataset_type == DatasetTypes.PET:

                # Plot Scalar data
                for i in range(min(data_dim, 10)):
                    if labels is not None:
                        plt.plot(times, mean_traj[key].cpu().detach().numpy()[:, i], label=labels[i], c=colors[i])
                    else:
                        plt.plot(times, mean_traj[key].cpu().detach().numpy()[:, i], c=colors[i])
                plt.xlabel('x')
                plt.legend()
                plt.savefig(os.path.join(output_dir, key+'_petscalar_average_trajectory.pdf'))
                plt.close()

                # Plot Pet Type
                # Get reals and reconsts
                reconsts_concatenated = [self._load_pet(reconst) for reconst in mean_traj[key].data.numpy()]

                reconsts_concatenated_0, reconsts_concatenated_1, reconsts_concatenated_2 = get_pet_slices(
                    reconsts_concatenated)

                res = np.concatenate([reconsts_concatenated_0,
                                      reconsts_concatenated_1,
                                      reconsts_concatenated_2], axis=0)

                res = (res - np.nanmean(res)) / np.nanstd(res)

                plt.clf()
                plt.imshow(res, cmap=plt.cm.YlOrBr)
                plt.axis('off')
                plt.savefig(os.path.join(output_dir, key +'_pet_average_trajectory.pdf'))
                plt.close()

                # Plot the decrease percentage over the time course
                var_rate = (mean_traj[key].data.numpy()[-1]-mean_traj[key].data.numpy()[0])/mean_traj[key].data.numpy()[0]
                pet_var_rate = self._load_pet(var_rate)

                reconsts_var_concatenated = np.concatenate([np.rot90(pet_var_rate[64, :, :]),
                                                            np.flip(np.rot90(pet_var_rate[:, 46, :]), axis=1),
                                                            np.flip(np.rot90(pet_var_rate[:, :, 41])[10:131, :], axis=1)], axis=1)
                plt.clf()
                plt.figure(figsize=(16, 8))
                plt.imshow(reconsts_var_concatenated, cmap=plt.cm.YlOrBr_r)
                plt.colorbar()
                plt.axis('off')
                plt.savefig(os.path.join(output_dir, key + 'np_pet_average_trajectory_var.pdf'))
                plt.close()

            else:
                trajectory = mean_traj[key]
                if data_dim == (64, 64, 64):
                    trajectory = torch.cat((trajectory[:, :, 25, :, :], trajectory[:, :, :, 32, :], trajectory[:, :, :, :, 32]))
                elif data_dim == (32, 32, 32):
                    trajectory = torch.cat((trajectory[:, :, 16, :, :], trajectory[:, :, :, 16, :], trajectory[:, :, :, :, 16]))
                save_image(trajectory, os.path.join(output_dir, key + '_average_trajectory.pdf'), nrow=10, normalize=True)

                del trajectory



    def get_time_shift_mean(self):
        return self.prior_parameters['time_shift_mean'].cpu().detach().numpy()[0]

    def get_eta_mean(self):
        return self.prior_parameters['eta_mean'].cpu().detach().numpy()[0]

    def get_time_shift_std(self):
        return torch.sqrt(self.prior_parameters['time_shift_log_variance'].exp()).cpu().detach().numpy()[0]

    def get_eta_std(self):
        return torch.sqrt(self.prior_parameters['eta_log_variance'].exp()).cpu().detach().numpy()[0]

    def save(self, output_dir):
        # print('Saving model:', self.mean_slope_norm)
        print('eta mean {}, eta std {}, time shift mean {}, time shift std {}'
              .format(self.get_eta_mean(),
                      self.get_eta_std(),
                      self.get_time_shift_mean(),
                      self.get_time_shift_std()))

        model_save_dir = os.path.join(output_dir, 'model')
        if not os.path.isdir(model_save_dir):
            os.mkdir(model_save_dir)

        prior_parameters_dict = {}
        for (key, val) in self.prior_parameters.items():
            prior_parameters_dict[key] = val.cpu().detach().numpy()

        pickle.dump((self.data_info, self.latent_space_dim, self.pre_encoder_dim,
                     #self.pre_decoder_dim,
                     self.use_cuda, self.random_slope, self.variational,
                     prior_parameters_dict),
                    open(os.path.join(model_save_dir, 'model_info.p'), 'wb'))

        if not self.monomodal:
            torch.save(self.merge_network.state_dict(), os.path.join(model_save_dir, 'merge_network.p'))
            #torch.save(self.pre_decoder.state_dict(), os.path.join(model_save_dir, 'pre_decoder.p'))
            if self.variational:
                torch.save(self.fc_merge.state_dict(),
                           os.path.join(model_save_dir, 'merge_layer_variances.p'))

        for key in self.encoders.keys():
            torch.save(self.encoders[key].state_dict(), os.path.join(model_save_dir, key + '_encoder.p'))
            torch.save(self.decoders[key].state_dict(), os.path.join(model_save_dir, key + '_decoder.p'))

    def compute_residuals(self, dataloader):
        """
        Encode and decode each subject, and save the mean squared error.
        """
        residuals = {}
        ids = {}
        ids_latent_positions = []
        for key in self.data_info.keys():
            residuals[key] = []
            ids[key] = []

        latent_positions = []


        latent_trajectories = torch.zeros(0)

        if self.use_cuda:
            latent_trajectories = latent_trajectories.cuda()  # cuda added

        if self.variational:
            latent_positions_variances = []

        cur_pos = 0

        for data in dataloader:
            if self.variational:
                mean, variances = self.encode(data)
            else:
                mean = self.encode(data)
            latent_positions.append(mean.detach().cpu().numpy())
            if self.variational:
                latent_positions_variances.append(variances.detach().cpu().numpy())
            ids_latent_positions.append(data['idx'])  # TODO: make this safer, looks insecure
            latent_traj = self.get_latent_trajectories(data, mean, log_variances=None, sample=False)
            for val in latent_traj.values():
                latent_trajectories = torch.cat([latent_trajectories, val], 0)
            reconstructed = self.decode(latent_traj)

            for key in reconstructed.keys():
                errors = ((reconstructed[key] - data[key]['values'].squeeze(0))**2.).view(len(latent_traj[key]), -1).cpu().detach().numpy()
                errors = np.mean(errors, axis=1)
                for error in errors:
                    residuals[key].append(error)
                    ids[key].append(data[key]['idx'])

        for key in self.data_info.keys():
            assert len(residuals[key]) == len(ids[key]), 'oops'

        if self.variational:
            return ids, residuals, np.array(latent_positions), np.array(latent_positions_variances), np.array(latent_trajectories.cpu().detach().numpy())
        else:
            return ids, residuals, np.array(latent_positions), np.array(latent_trajectories.cpu().detach().numpy())

    def extra_writing(self, output_dir, dataloader, dico):
        pass


def load_model(folder, depth="normal"):
    (data_info, latent_space_dim, pre_encoder_dim,# pre_decoder_dim,
     use_cuda, random_slope, variational,
     prior_parameters_dict) = pickle.load(open(os.path.join(folder, 'model_info.p'), 'rb'))

    model = RandomSlopeModel(data_info=data_info, latent_space_dim=latent_space_dim,
                             pre_encoder_dim=pre_encoder_dim,# pre_decoder_dim=pre_decoder_dim,
                             use_cuda=False, random_slope=random_slope,
                             variational=variational, depth=depth)

    model.set_prior_parameters(prior_parameters_dict)

    if not model.monomodal:
        model.merge_network.load_state_dict(torch.load(os.path.join(folder, 'merge_network.p'),
                                                  map_location=lambda storage, loc: storage))
        #model.pre_decoder.load_state_dict(torch.load(os.path.join(folder, 'pre_decoder.p'),
        #                                          map_location=lambda storage, loc: storage))

        if model.variational:
            model.fc_log_variances.load_state_dict(torch.load(os.path.join(folder, 'merge_layer_variances.p'),
                                                              map_location=lambda storage, loc: storage))

    for key in model.encoders.keys():
        model.encoders[key].load_state_dict(torch.load(os.path.join(folder, key + '_encoder.p'),
                                                       map_location=lambda storage, loc: storage))
        model.decoders[key].load_state_dict(torch.load(os.path.join(folder, key + '_decoder.p'),
                                                       map_location=lambda storage, loc: storage))

    return model


