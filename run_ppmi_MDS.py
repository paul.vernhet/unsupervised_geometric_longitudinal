import os
from longitudinal_scalar_dataset import LongitudinalScalarDataset
from random_slope_model import RandomSlopeModel
# from estimate_random_slope_model import estimate_random_slope_model
from estimate_adversarial import estimate_random_slope_model_adversarial
import pickle as pickle
import numpy as np
import pandas as pd
from utils import plot_sampler_pca
from samplers import NormalSampler


data_dir = '../data_ppmi'
output_dir = '../output_ppmi_MDS'

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

data = pd.read_csv(os.path.join(data_dir, 'df_allscores_allcohorts.csv'))

# Selecting PD subjects:
input = data.loc[data['ENROLL_CAT'] == 'PRODROMA']
input = input[['Subject Identifier', 'MDS_Rigidity', 'MDS_Bradykinesia', 'MDS_VoiceFace', 'MDS_Gait', 'MDS_sum', 'Age at Visit']]
input = input.dropna()

input = input.values
ids = input[:, 0]
values = input[:, 1:6]
times = input[:, -1]

labels = {}
for rid in ids:
    labels[rid] = 0

print(ids.shape, values.shape, times.shape)

dataset = LongitudinalScalarDataset(
    ids,
    values,
    times
)

latent_space_dim = 2
random_slope = False

random_slope_model = RandomSlopeModel(
    data_dim=5,
    latent_space_dim=latent_space_dim,
    is_image=False,
    encoder_hidden_dim=32,
    decoder_hidden_dim=32,
    labels=['MDS_Rigidity', 'MDS_Bradykinesia', 'MDS_VoiceFace', 'MDS_Gait', 'MDS_sum'],
    initial_slope_norm=4.5,
    random_slope=random_slope
)


pickle.dump((dataset, labels), open(os.path.join(output_dir, 'dataset_info.p'), 'wb'))

sampler = NormalSampler(dimension=random_slope_model.encoder_dim)

plot_sampler_pca(sampler, os.path.join(output_dir, 'prior_distribution.pdf'))

estimate_random_slope_model_adversarial(random_slope_model, dataset, n_epochs=10000,
                             learning_rate=5e-5, output_dir=output_dir,
                             batch_size=16, save_every_n_iters=50, sampler=sampler)


